package com.sg.selenium;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

/**
 * Created by amitdatta on 07/12/16.
 */

@Test
public class Selenium07Dec1 {

    WebDriver driver;

    @BeforeTest
    public void setup() throws Exception{

        driver = new FirefoxDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.get("http://www.github.com");
    }

    public void getExe() throws  InterruptedException{

        WebElement search_box = driver.findElement(By.xpath("//input[@name='q']"));
        search_box.sendKeys("Hello");
        search_box.sendKeys(Keys.ENTER);
        Thread.sleep(5000);
        search_box.clear();
    }

}
