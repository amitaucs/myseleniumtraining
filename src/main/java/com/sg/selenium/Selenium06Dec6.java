package com.sg.selenium;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.util.Iterator;
import java.util.Set;

/**
 * Created by amitdatta on 06/12/16.
 */
public class Selenium06Dec6 {

    public static void main(String args[]) {

        String url ="https://secure.yatra.com/social/common/yatra/signin.htm";
        WebDriver d = new FirefoxDriver();
        d.manage().window().maximize();

        d.navigate().to(url);
        //d.findElement(By.xpath("//a[@class='fk-button-social social_fb rmargin10'])[1]")).click();

        d.findElement(By.id("facebookSignIn")).click();

        Set<String> sa = d.getWindowHandles();
        Iterator<String> lst = sa.iterator();
        String parent = lst.next();
        String child = lst.next();

        d.switchTo().window(child);
        //d.switchTo().defaultContent(); ---switch to parent

    }
}
