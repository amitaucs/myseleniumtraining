package com.sg.selenium;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

/**
 * Created by amitdatta on 06/12/16.
 */
public class Selenium06Dec3 {

    public static void main(String args[]) throws Exception{

        System. setProperty("webdriver.chrome.driver", "/users/amitdatta/Amit_Work/chromedriver 2");
        WebDriver d = new ChromeDriver();

        d.manage().window().maximize();
        d.get("http://www.irctc.com/Emp_Login.jsp");

        //d.findElement(By.xpath("//input [@class='go']")).click();
        d.findElement(By.name("Login")).click();

        Alert alt = d.switchTo().alert();
        alt.accept();

    }
}
