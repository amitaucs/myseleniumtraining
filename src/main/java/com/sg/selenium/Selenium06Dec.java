package com.sg.selenium;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.interactions.Actions;

/**
 * Created by amitdatta on 06/12/16.
 */
public class Selenium06Dec {

    public static void main(String[] args) {

        WebDriver d = new FirefoxDriver();
        d.get("http://www.dhtmlgoodies.com/submitted-scripts/i-google-like-drag-drop/index.html");
        WebElement source = d.findElement(By.xpath("//h1[text()='Block 1']"));
        WebElement destination = d.findElement(By.xpath("//h1[text()='Block 3']"));

        Actions a = new Actions(d);
        a.dragAndDrop(source,destination).perform();

    }

}
