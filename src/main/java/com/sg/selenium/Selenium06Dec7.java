package com.sg.selenium;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

/**
 * Created by amitdatta on 06/12/16.
 */
public class Selenium06Dec7 {

    public static void main(String args[]){

        WebDriver d = new FirefoxDriver();
        d.get("/users/amitdatta/IdeaProjects/myseleniumtraining/src/main/resources/seleniumdec2.html");
        d.findElement(By.id("t1")).sendKeys("abc");

        WebElement e = d.findElement(By.className("c1"));
        d.switchTo().frame(e);
        d.findElement(By.id("t2")).sendKeys("xyz");
        d.switchTo().parentFrame();
        d.findElement(By.id("t1")).clear();
        d.findElement(By.id("t1")).sendKeys("pra");



    }
}
