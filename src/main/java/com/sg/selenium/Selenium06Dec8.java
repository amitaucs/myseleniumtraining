package com.sg.selenium;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.events.EventFiringWebDriver;


import java.io.File;
import java.io.IOException;


/**
 * Created by amitdatta on 06/12/16.
 */
public class Selenium06Dec8 {

    public static void main(String args[]) throws IOException {

        WebDriver d = new FirefoxDriver();
        d.manage().window().maximize();
        d.get("https://www.facebook.com");
        EventFiringWebDriver e = new EventFiringWebDriver(d);
        File srcFile = e.getScreenshotAs(OutputType.FILE);

        File destFile = new File("/users/amitdatta/Amit_Work/selenium.png");
        FileUtils.copyFile(srcFile,destFile);


    }
}
