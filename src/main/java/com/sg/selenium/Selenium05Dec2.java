package com.sg.selenium;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;

/**
 * Created by amitdatta on 05/12/16.
 */
public class Selenium05Dec2 {

    public static void main(String[] args) {
        WebDriver d = new FirefoxDriver();

        d.manage().window().maximize();

        String url = "http://www.myntra.com";

        d.get(url);
        WebElement x = d.findElement(By.xpath("(//*[@class='desktop-main'])[1]"));
        Actions a = new Actions(d);
        //a.moveToElement(x).perform();
        //d.findElement(By.xpath("(//a[@class='desktop-categoryLink'])[13]")).click();

        a.contextClick(x).perform();
        d.findElement(By.xpath("(//a[@class='desktop-categoryLink'])[13]"));
        //a.sendKeys("t").perform();//open in new tab
        a.sendKeys("w").perform();//open in new window
    }
}
