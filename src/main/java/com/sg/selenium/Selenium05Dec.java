package com.sg.selenium;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.safari.SafariDriver;
import org.openqa.selenium.support.ui.Select;

import java.util.List;

/**
 * Created by amitdatta on 05/12/16.
 */
public class Selenium05Dec {

    public static void main(String[] args) {
        WebDriver d = new FirefoxDriver();

       // d.manage().window().maximize();

        String url = "file:///C:/Users/admin/workspace/Selenium1/bin/selenium.html";
        String url1 = "https://www.facebook.com";
        String url2 = "http://www.seleniumhq.org/download";

        d.manage().deleteAllCookies();
        d.navigate().to(url1);
        //d.findElement(By.id("email")).sendKeys("spoorthy.dg@gmail.com");
        //d.findElement(By.tagName("input")).sendKeys("spoorthy.dg@gmail.com");
        //d.findElement(By.className("inputtext")).sendKeys("spoorthy.dg@gmail.com");
        //d.findElement(By.name("email")).sendKeys("spoorthy.dg@gmail.com");
        //d.findElement(By.xpath("//*[@id='email']")).sendKeys("spoorthy.dg@gmail.com");
        //d.findElement(By.cssSelector("#email")).sendKeys("spoorthy.dg@gmail.com");
        //d.findElement(By.id("u_0_l")).click();

        //d.findElement(By.linkText("Forgotten account?")).click();
        //d.findElement(By.partialLinkText("Forgotten")).click();

        //d.findElement(By.xpath("/html/body/div[4]/input")).sendKeys("spoorthy.dg@gmail.com");//absolute xpth
        //d.findElement(By.xpath("//*[@id='email']")).sendKeys("spoorthy.dg@gmail.com");//xpath by attribute
        //d.findElement(By.xpath(".//a[text()='Forgotten account?']")).click();//xpath by text
        //d.findElement(By.xpath("//span[contains(text(),'Create Jobs')]")).click();//xpath by contain
        //d.findElement(By.xpath("//a[contains(text(),'Forgotten')]")).click();//xpath by contain
        //d.findElement(By.xpath("//label[contains(@id,'loginbutton')]")).click();//xpath by contain
        //d.findElement(By.xpath("//td[(text()='Java')]/../td[4]")).click();//xpath by dependent - independent
        //d.findElement(By.xpath("(//input)[3]")).sendKeys("spoorthy");//xpath by group index
        //d.findElement(By.id("u_0_l")).click();

        d.findElement(By.id("u_0_i")).click();
        d.findElement(By.id("month")).sendKeys("Apr");

        WebElement ele = d.findElement(By.id("day"));
        Select sel = new Select(ele);
        sel.selectByIndex(7);

		/* three types of selecting list values |^ */
        new Select(d.findElement(By.id("year"))).selectByIndex(6);
        new Select(d.findElement(By.id("day"))).selectByValue("11");//option11 starts with option0
        new Select(d.findElement(By.id("month"))).selectByVisibleText("Jun");//case sensitive

        WebElement ele1 = d.findElement(By.id("year"));
        Select sel1 = new Select(ele1);
        List<WebElement> li = sel1.getOptions();
        int count = li.size();
        for (int i = 0; i < count; i++) {
            sel1.selectByIndex(i);
        }

        System.out.println(d.findElement(By.id("email")).getText());
    }
}
